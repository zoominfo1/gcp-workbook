resource "google_container_registry" "esd-registry" {

}

module "cluster" {
  source                     = "../modules/compute-gke"
  cluster_name               = var.cluster_name
  project_region             = "${var.project_region}-c"
  cluster_initial_node_count = 1
  cluster_man_rules          = var.cluster_man_rules
  network                    = module.network.vpc_id
  subnetwork                 = module.subnet.vpc_subnet_name
  identity_namespace         = "${var.project_id}.svc.id.goog"
}

module "nodepool" {
  source                     = "../modules/compute-gke-nodepool"
  cluster_node_pool_name     = var.cluster_node_pool_name
  cluster_name               = module.cluster.id
  sa_email                   = google_service_account.gke_sa.email
  cluster_initial_node_count = var.cluster_initial_node_count
}

resource "kubernetes_service_account" "ksa" {
  metadata {
    name = var.ksa_name
    annotations = {
      "iam.gke.io/gcp-service-account" = "${google_service_account.gke_sa.email}"
    }
  }
  depends_on = [module.nodepool]
}

# Enable the IAM binding between your YOUR-GSA-NAME and YOUR-KSA-NAME:
resource "google_service_account_iam_binding" "gsa-ksa-binding" {
  service_account_id = google_service_account.gke_sa.name //add the resource linking for service account that you create
  role               = "roles/iam.workloadIdentityUser"

  members = [
    "serviceAccount:${var.project_id}.svc.id.goog[default/${var.ksa_name}]"
  ]
  depends_on = [kubernetes_service_account.ksa]
}

# resource "null_resource" "deploy_scripts1" {
#   provisioner "local-exec" {
#     working_dir = path.module
#     command     = "bash deploy-ingress-config.sh ${var.project_id} ${var.cluster_name} ${var.project_region}"
#   }

#   depends_on = [
#     module.nodepool
#   ]
# }