resource "google_service_account" "gke_sa" {
  account_id   = var.gke_sa_name
  display_name = "GKE Cluster Service Account"
}


resource "google_project_iam_member" "gke_sa_user" {
  project = var.project_id
  role    = "roles/iam.serviceAccountUser"
  member  = "serviceAccount:${google_service_account.gke_sa.email}"
}

resource "google_project_iam_member" "gke_storage_admin" {
  project = var.project_id
  role    = "roles/storage.admin"
  member  = "serviceAccount:${google_service_account.gke_sa.email}"
}

resource "google_project_iam_member" "gke_editor" {
  project = var.project_id
  role    = "roles/editor"
  member  = "serviceAccount:${google_service_account.gke_sa.email}"
}



resource "google_service_account" "remote_builders_sa" {
  project      = var.project_id
  account_id   = var.remote_builders_sa_name
  display_name = "Remote builders Service Account"
}

resource "google_project_iam_member" "rb_storage_admin" {
  project = var.project_id
  role    = "roles/storage.admin"
  member  = "serviceAccount:${google_service_account.remote_builders_sa.email}"
}

resource "google_project_iam_member" "rb_container_admin" {
  project = var.project_id
  role    = "roles/container.admin"
  member  = "serviceAccount:${google_service_account.remote_builders_sa.email}"
}

resource "google_project_iam_member" "rb_sa_user" {
  project = var.project_id
  role    = "roles/iam.serviceAccountUser"
  member  = "serviceAccount:${google_service_account.remote_builders_sa.email}"
}

#Data block to retrieve the project number
data "google_project" "project" {
  project_id = var.project_id
}

#Cloud Build Service account
# data "google_service_account" "cloudbuild-account" {
#   account_id = "${data.google_project.project.number}@cloudbuild.gserviceaccount.com"
# }


# resource "google_project_iam_member" "cloud-build" {
#   project = var.project_id
#   role    = "roles/cloudbuild.builds.builder"
#   member  = "serviceAccount:${data.google_project.project.number}@cloudbuild.gserviceaccount.com"
# }

# resource "google_project_iam_member" "cloud-build-private-pool" {
#   project = var.project_id
#   role    = "roles/cloudbuild.workerPoolUser"
#   member  = "serviceAccount:${data.google_project.project.number}@cloudbuild.gserviceaccount.com"
# }

# resource "google_project_iam_member" "sa-user" {
#   project = var.project_id
#   role    = "roles/iam.serviceAccountUser"
#   member  = "serviceAccount:${data.google_project.project.number}@cloudbuild.gserviceaccount.com"
# }

# resource "google_project_iam_member" "compute-admin" {
#   project = var.project_id
#   role    = "roles/compute.admin"
#   member  = "serviceAccount:${data.google_project.project.number}@cloudbuild.gserviceaccount.com"
# }

# resource "google_project_iam_member" "storage-admin" {
#   project = var.project_id
#   role    = "roles/storage.admin"
#   member  = "serviceAccount:${data.google_project.project.number}@cloudbuild.gserviceaccount.com"
# }

# resource "google_project_iam_member" "k8s-admin" {
#   project = var.project_id
#   role    = "roles/container.admin"
#   member  = "serviceAccount:${data.google_project.project.number}@cloudbuild.gserviceaccount.com"
# }
