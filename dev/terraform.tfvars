project_id      = "shriram-poc-project1"
service_account = "sa-key.json"

vpc_name                    = "gke-network"
vpc_auto_create_subnetworks = false

subnet_name                     = "subnet-1"
subnet_ip_cidr_range            = "10.0.1.0/24"
subnet_region                   = "us-central1"
subnet_description              = "sample"
subnet_secondary_ip_range       = { "vpc_subnet_secondary_ip_needed" = false }
subnet_private_ip_google_access = true
subnet_log_config               = { "enable_flow_logs" = true }

router_name   = "new-router"
router_region = "us-central1"

address_name = "my-address1"


nat_name                           = "my-nat"
source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"
nat_ip_allocate_option             = "AUTO_ONLY"
nat_subnetwork = {

}

bucket_name = "test"

object_name = "test-object.csv"

cluster_man_rules = [{
  "cidr_block" : "103.252.5.228/32",
  "display_name" : "shriIP"
  },
  {
    "cidr_block" : "34.16.125.139/32",
    "display_name" : "privatepool"
  }
]    