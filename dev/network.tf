module "network" {
  source                      = "../modules/compute-network"
  vpc_name                    = var.vpc_name
  vpc_auto_create_subnetworks = false
}

module "subnet" {
  source                              = "../modules/compute-subnetwork"
  vpc_subnet_name                     = var.subnet_name
  vpc_subnet_ip_cidr_range            = var.subnet_ip_cidr_range
  vpc_subnet_region                   = var.subnet_region
  vpc_subnet_description              = var.subnet_description
  vpc_self_link                       = module.network.vpc_self_link
  vpc_subnet_secondary_ip_range       = var.subnet_secondary_ip_range
  vpc_subnet_private_ip_google_access = var.subnet_private_ip_google_access
  vpc_subnet_log_config               = var.subnet_log_config
}

module "router" {
  source         = "../modules/compute-router"
  router_name    = var.router_name
  router_network = module.network.vpc_self_link
  router_region  = var.router_region
}

resource "google_compute_global_address" "ingress-ip" {
  name = var.address_name
}

module "nat" {
  source                             = "../modules/compute-router-nat"
  nat_name                           = var.nat_name
  nat_router_name                    = module.router.router_name
  nat_router_region                  = module.router.router_region
  source_subnetwork_ip_ranges_to_nat = var.source_subnetwork_ip_ranges_to_nat
  nat_ip_allocate_option             = var.nat_ip_allocate_option
  nat_subnetwork = {
    name        = module.subnet.vpc_subnet_name
    ip_ranges   = ["ALL_IP_RANGES"]
    range_names = []
  }
}
