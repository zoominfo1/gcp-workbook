resource "kubernetes_deployment_v1" "deployment" {
  metadata {
    name = var.deployment_name
    labels = {
      app = var.deployment_name
    }
  }
  spec {
    replicas = 1
    selector {
      #match labels help service attach to the deployment
      match_labels = {
        app = var.deployment_name
      }
    }
    template {
      metadata {
        labels = {
          app = var.deployment_name
        }
      }
      spec {
        service_account_name = var.ksa_name
        container {
          image = "nginx:latest"
          name  = "image"
          env {
            name = "INSTANCE_CONNECTION_NAME"
            value = var.instance_connection_name
          }
          env {
            name = "DB_USER"
            value = var.db_user
          }
          env {
            name = "DB_PASS"
            value = var.db_pass
          }
          env {
            name = "DB_NAME"
            value = var.db_name
          }
          port {
            protocol       = "TCP"
            container_port = 80
          }
          resources {
            requests = {
              cpu    = "100m"
              memory = "100Mi"
            }
          }


        }
      }


    }
  }
  lifecycle {
    ignore_changes = [ spec[0].template[0].spec[0].container[0].image,
                       spec[0].template[0].spec[0].container[0].env[0],
                       spec[0].template[0].spec[0].container[0].env[1],
                       spec[0].template[0].spec[0].container[0].env[2],
                       spec[0].template[0].spec[0].container[0].env[3]
                        ]
  }
  depends_on = [kubernetes_service_account.ksa]
}

#Creating Nodeport service for Integration Service deployment
resource "kubernetes_service_v1" "nodeport" {
  metadata {
    name = "nodeport"
  }
  spec {
    selector = {
      app = var.deployment_name
    }
    port {
      port        = 80
      target_port = 80
      protocol    = "TCP"
    }

    type = "NodePort"
  }
  depends_on = [kubernetes_deployment_v1.deployment]
}

#Creating Ingress service for Integration Service deployment
resource "kubernetes_ingress_v1" "ingress" {
  #   wait_for_load_balancer = true
  metadata {
    name = var.ingress_name
    annotations = {
      "networking.gke.io/v1beta1.FrontendConfig" = "frontend-config"
      "kubernetes.io/ingress.global-static-ip-name"     = var.address_name
      "ingress.kubernetes.io/ssl-redirect"              = "true"
      "kubernetes.io/ingress.allow-http"                = "false"
      "networking.gke.io/managed-certificates"          = "cert-production"
      "nginx.ingress.kubernetes.io/affinity"            = "cookie"
      "nginx.ingress.kubernetes.io/session-cookie-name" = "route"
      "nginx.ingress.kubernetes.io/session-cookie-hash" = "sha1"
    }
  }
  spec {
    rule {
      host = "pythonapplication.endpoints.${var.project_id}.cloud.goog"
      http {
        path {
          path = "/*"
          backend {
            service {
              name = "nodeport"
              port {
                number = 80
              }
            }
          }
        }
      }
    }
  }
  depends_on = [kubernetes_service_v1.nodeport]
}

resource "kubernetes_horizontal_pod_autoscaler_v1" "hpa" {
  metadata {
    name = "hpa"
  }
  spec {
    scale_target_ref {
      api_version = "apps/v1"
      kind        = "Deployment"
      name        = var.deployment_name
    }

    min_replicas = 2
    max_replicas = 100
  }
  depends_on = [kubernetes_deployment_v1.deployment]
}

resource "google_compute_ssl_policy" "custom_ssl_policy" {
  name            = var.ssl_policy_name
  min_tls_version = var.ssl_tls_version
  profile         = var.ssl_profile
  custom_features = var.ssl_custom_features
}
