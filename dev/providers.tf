//The provider is used to specify the platform to be used, here it is gcp 

provider "google" {
  project     = var.project_id
  region      = var.project_region
  zone        = "${var.project_region}-c"
}

provider "google-beta" {
  project     = var.project_id
  region      = var.project_region
}

# data "google_container_cluster" "esd_cluster_data" {
#     name = var.cluster_name
#     location = "${var.project_region}-c"

#     depends_on = [
#       google_container_cluster.cluster
#     ]
# }

data "google_client_config" "access_token" {}


provider "kubernetes" {
  host                   = "https://${module.cluster.endpoint}"
  token                  = data.google_client_config.access_token.access_token
  client_certificate     = base64decode(module.cluster.client_certificate)
  client_key             = base64decode(module.cluster.client_key)
  cluster_ca_certificate = base64decode(module.cluster.ca_certificate)
}