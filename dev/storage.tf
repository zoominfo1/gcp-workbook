resource "random_string" "random" {
  length    = 10
  min_lower = 10
  numeric   = false
  special   = false
}

module "bucket" {
  source = "../modules/storage-bucket"
  name   = "${var.bucket_name}-${random_string.random.result}"
}

module "object" {
  source      = "../modules/storage-object"
  bucket_name = module.bucket.bucket_name
  object_name = var.object_name
  source_path = "./test.csv"
}

