locals {
  timestamp = regex_replace(timestamp(), "[- TZ:]", "")
}

variable "project_id" {
    type =  string
    default = "shriram-poc-project1"
}

variable "zone" {
    type = string
    default = "us-central1-b"
}

source "googlecompute" "terraform-agent-image" {
  project_id          = var.project_id
  zone                = var.zone
  source_image_family = "debian-11"
  image_name          = "packer-terraform-${local.timestamp}"
  image_description   = "Terraform Agent Image"
  ssh_username        = "packer"
  tags                = ["packer"]
}

build {
  sources = ["sources.googlecompute.terraform-agent-image"]

  hcp_packer_registry {
    bucket_name = "packer-terraform-images"
    description = "Bucket used to store terraform images metadata"
  }

  provisioner "shell" {
    script = "packer/install-tf.sh"
    }
}