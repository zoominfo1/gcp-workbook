
resource "google_compute_router" "router" {
  name        = var.router_name
  network     = var.router_network
  description = var.router_description
  region      = var.router_region

  dynamic "bgp" {
    for_each = lookup(var.router_bgp_details, "bgp_required", false) ? [{
      asn               = lookup(var.router_bgp_details, "bgp_asn", 64514)
      advertise_mode    = lookup(var.router_bgp_details, "bgp_advertise_mode", "DEFAULT")
      advertised_groups = lookup(var.router_bgp_details, "bgp_advertised_groups", "")
      # advertised_ip_ranges = lookup(var.router_bgp_details, "bgp_advertised_ip_ranges", {"range" = "10.0.0.0/16"} )
    }] : []

    content {
      asn               = bgp.value.asn
      advertise_mode    = bgp.value.advertise_mode
      advertised_groups = (bgp.value.advertise_mode == "CUSTOM") ? bgp.value.advertised_groups : null
      # advertised_ip_ranges = (bgp.value.advertise_mode == "CUSTOM") ? bgp.value.advertised_ip_ranges : null

    }
  }
}