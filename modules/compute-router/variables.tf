variable "router_name" {
  type        = string
  description = "The name of the router"
}

variable "router_network" {
  type        = string
  description = "The network for which the router has to be created"
}

variable "router_region" {
  type        = string
  description = "The region in which router is to be created"
}

variable "router_description" {
  type        = string
  description = "Description of the router"
  default     = "Router description"
}

variable "router_bgp_details" {
  type        = map(string)
  description = "Config for bgp setup of the router"
  default     = {}
}