variable "nat_name" {
  type        = string
  description = "Name of the NAT"
}

variable "nat_router_name" {
  type        = string
  description = "Name of the router associated with the NAT"
}

variable "nat_router_region" {
  type        = string
  description = "The region of the NAT"
}

variable "source_subnetwork_ip_ranges_to_nat" {
  type        = string
  description = "The Source subnet ip ranges to nat mapping"
}

variable "nat_ip_allocate_option" {
  type        = string
  description = "NAT IP allocation option"
}

variable "nat_ips" {
  type        = list(string)
  description = "The NAT IP ranges"
  default     = []
}

variable "drain_nat_ips" {
  type        = list(string)
  description = "Drain NAT IPs"
  default     = []
}

variable "nat_subnetwork" {
  type = object({
    name : optional(string),
    ip_ranges : optional(list(string)),
    range_names : optional(list(string))
  })
  description = "Subnets for NAT"
  default     = {}
}

variable "nat_min_ports" {
  type        = number
  description = "Min Ports for NAT"
  default     = 64
}

variable "nat_udp_idle_timeout" {
  type        = number
  description = "NAT UDP Idle timeout"
  default     = 30
}

variable "icmp_idle_timeout" {
  type        = number
  description = "ICMP Idle timeout"
  default     = 30
}

variable "tcp_established_timeout" {
  type        = number
  description = "TCP established timeout "
  default     = 1200
}

variable "tcp_transitory_timeout" {
  type        = number
  description = "TCP transitory timeout"
  default     = 30
}

variable "nat_log_config" {
  type        = map(string)
  description = "Log config of NAT"
  default     = {}
}