resource "google_compute_router_nat" "nat" {
  name                               = var.nat_name
  router                             = var.nat_router_name
  region                             = var.nat_router_region
  source_subnetwork_ip_ranges_to_nat = var.source_subnetwork_ip_ranges_to_nat
  # nat_ip_allocate_option             = (var.source_subnetwork_ip_ranges_to_nat == "LIST_OF_SUBNETWORKS") ? "MANUAL_ONLY" : var.nat_ip_allocate_option
  nat_ip_allocate_option = var.nat_ip_allocate_option
  nat_ips                = var.nat_ip_allocate_option == "MANUAL_ONLY" ? var.nat_ips : null
  # drain_nat_ips = var.drain_nat_ips

  dynamic "subnetwork" {
    for_each = (var.source_subnetwork_ip_ranges_to_nat == "LIST_OF_SUBNETWORKS") ? [{
      name                     = lookup(var.nat_subnetwork, "name", "default-name")
      source_ip_ranges_to_nat  = lookup(var.nat_subnetwork, "ip_ranges", ["ALL_IP_RANGES"])
      secondary_ip_range_names = lookup(var.nat_subnetwork, "range_names", [])
    }] : []

    content {
      name                     = subnetwork.value.name
      source_ip_ranges_to_nat  = subnetwork.value.source_ip_ranges_to_nat
      secondary_ip_range_names = contains(subnetwork.value.source_ip_ranges_to_nat, "LIST_OF_SECONDARY_IP_RANGES") ? subnetwork.value.secondary_ip_range_names : null
    }
  }

  min_ports_per_vm                 = var.nat_min_ports
  udp_idle_timeout_sec             = var.nat_udp_idle_timeout
  icmp_idle_timeout_sec            = var.icmp_idle_timeout
  tcp_established_idle_timeout_sec = var.tcp_established_timeout
  tcp_transitory_idle_timeout_sec  = var.tcp_transitory_timeout

  dynamic "log_config" {
    for_each = lookup(var.nat_log_config, "log_enable", false) ? [{
      enable = true
      filter = lookup(var.nat_log_config, "log_filter", "ALL")
    }] : []

    content {
      enable = log_config.value.enable
      filter = log_config.value.filter
    }
  }
}