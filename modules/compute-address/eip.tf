resource "google_compute_address" "address" {
  # count = var.eip_count
  name         = var.address_name
  address_type = var.address_type
  description  = var.description
  purpose      = var.address_type == "INTERNAL" ? var.address_purpose : null
  address      = var.address_type == "INTERNAL" ? var.address_value : null
  network_tier = var.address_network_tier
  subnetwork   = var.address_type == "INTERNAL" ? (var.address_purpose == "GCP_ENDPOINT" || var.address_purpose == "DNS_RESOLVER") ? var.address_subnetwork : null : null
  # labels = var.address_labels
  region = var.address_region

}