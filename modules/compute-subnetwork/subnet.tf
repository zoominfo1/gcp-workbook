resource "google_compute_subnetwork" "vpc_subnetwork" {
  name          = var.vpc_subnet_name
  ip_cidr_range = var.vpc_subnet_ip_cidr_range
  region        = var.vpc_subnet_region
  network       = var.vpc_self_link
  description   = var.vpc_subnet_description

  dynamic "secondary_ip_range" {
    for_each = lookup(var.vpc_subnet_secondary_ip_range, "vpc_subnet_secondary_ip_needed", false) ? [{
      ip_cidr_range = lookup(var.vpc_subnet_secondary_ip_range, "secondary_ip_range", "192.168.1.0/26")
      range_name    = lookup(var.vpc_subnet_secondary_ip_range, "range_name", "default-range")
    }] : []
    content {
      ip_cidr_range = secondary_ip_range.value.ip_cidr_range
      range_name    = secondary_ip_range.value.range_name
    }
  }
  private_ip_google_access = var.vpc_subnet_private_ip_google_access
  dynamic "log_config" {
    for_each = lookup(var.vpc_subnet_log_config, "enable_flow_logs", false) ? [{
      aggregation_interval = lookup(var.vpc_subnet_log_config, "subnet_flow_logs_interval", "INTERVAL_5_SEC")
      flow_sampling        = lookup(var.vpc_subnet_log_config, "subnet_flow_logs_sampling", "0.5")
      metadata             = lookup(var.vpc_subnet_log_config, "subnet_flow_logs_metadata", "INCLUDE_ALL_METADATA")
    }] : []
    content {
      aggregation_interval = log_config.value.aggregation_interval
      flow_sampling        = log_config.value.flow_sampling
      metadata             = log_config.value.metadata
    }
  }
} 