# Variables for Subnet

variable "vpc_subnet_name" {
  type        = string
  description = "Names of the subnets to be created"
}

variable "vpc_subnet_ip_cidr_range" {
  type        = string
  description = "CIDR range of the subnets to be created"
}

variable "vpc_subnet_region" {
  type        = string
  description = "Region where the subnets should be created"
}

variable "vpc_self_link" {
  type        = string
  description = "The self link of the VPC in which the subnet has to be created"
}

variable "vpc_subnet_description" {
  type        = string
  description = "Description for the subnet"
  default     = ""

}

variable "vpc_subnet_secondary_ip_range" {
  type        = map(string)
  description = "Secondary IP range for the subnets if required"
  default     = {}
}

variable "vpc_subnet_private_ip_google_access" {
  type        = bool
  description = "Whether private IP google access is required"
  default     = true
}

variable "vpc_subnet_log_config" {
  type        = map(string)
  description = "The configurations for logging of the subnet"
  default     = {}
}

# variable "subnet_secondary_ip_ranges" {
#     default = [{"vpc_subnet_secondary_ip_needed" = true, "subnet_secondary_ip_info" = [ {"secondary_ip_range" = "13.0.1.0/24", "range_name" = "range"} ]}, {"vpc_subnet_secondary_ip_needed" = true, "subnet_secondary_ip_info" = [ {"secondary_ip_range" = "13.0.1.0/24", "range_name" = "range"}, {"secondary_ip_range" = "13.0.2.0/24", "range_name" = "range"} ]}]
# }