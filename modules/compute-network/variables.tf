variable "vpc_name" {
  type        = string
  description = "Name of the VPC"
}

variable "vpc_description" {
  type        = string
  description = "A description for the VPC"
  default     = "VPC Description"
}
# Variables for VPC
variable "vpc_auto_create_subnetworks" {
  type        = bool
  description = "Whether to auto create subnets in every region"
  default     = true
}

variable "vpc_routing_mode" {
  type        = string
  description = "The routing mode for the VPC"
  default     = "REGIONAL"
}

variable "vpc_delete_default_routes_on_create" {
  type        = bool
  description = "Whether the default routes should be deleted on create"
  default     = false
}

