variable "name" {
  type        = string
  description = "Name of the bucket"
}

variable "location" {
  type        = string
  description = "The location of the bucket"
  default     = "NAM4"
}

variable "storage_class" {
  type        = string
  description = "The storage class of the bucket"
  default     = "MULTI_REGIONAL"
}

variable "versioning" {
  type        = bool
  description = "Whether versioning should be enabled"
  default     = true
}
