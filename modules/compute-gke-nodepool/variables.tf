variable "cluster_node_pool_name" {
  type        = string
  description = "Name of the node pool"
}

variable "cluster_name" {
  type        = string
  description = "Name of the cluster to create the nodepool in"
}

variable "sa_email" {
  type        = string
  description = "Email of the service account to be attached"
}

variable "cluster_machine_type" {
  type        = string
  description = "Machine type of the node pool VMs"
  default     = "n1-standard-1"
}

variable "cluster_tags" {
  type        = list(string)
  description = "Tags to be associated with the Node pool"
  default     = ["remote-builders"]
}

variable "cluster_initial_node_count" {
  type        = number
  description = "Number of initial nodes to be created"
  default     = 3
}