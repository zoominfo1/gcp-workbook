variable "bucket_name" {
  type        = string
  description = "Name of the bucket in which object has to be created"
}

variable "object_name" {
  type        = string
  description = "Name of the object to be created"
}

variable "source_path" {
  type        = string
  description = "Path/Location of the file to be uploaded as the object"
}