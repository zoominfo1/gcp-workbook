variable "cluster_name" {
  type        = string
  description = "Name of the cluster"
}

variable "cluster_description" {
  type        = string
  description = "Description of the cluster"
  default     = "GKE Cluster"
}

variable "cluster_binary_authorization" {
  type    = bool
  default = true
}

variable "cluster_shielded_nodes" {
  type    = bool
  default = true
}

variable "cluster_initial_node_count" {
  type    = number
  default = 5
}

variable "cluster_networking_mode" {
  type    = string
  default = "VPC_NATIVE"
}

variable "cluster_ip4_cidr" {
  type    = string
  default = ""
}

variable "services_ipv4_cidr_block" {
  type    = string
  default = ""
}

variable "cluster_man_needed" {
  type    = bool
  default = true
}

variable "cluster_man_rules" { //override
  type = list(map(string))
}

variable "cluster_machine_type" {
  type    = string
  default = "e2-standard-2"
}

variable "cluster_tags" {
  type    = list(string)
  default = ["remote-builders"]
}

variable "cluster_enable_private_nodes" {
  type    = bool
  default = true
}

variable "cluster_enable_private_endpoint" {
  type    = bool
  default = false
}

variable "cluster_master_ipv4_cidr_block" {
  type    = string
  default = "172.16.0.0/28"
}

variable "project_region" {
  type = string
}

variable "network" {
  type = string
}

variable "subnetwork" {
  type = string
}

variable "identity_namespace" {
  type = string
}