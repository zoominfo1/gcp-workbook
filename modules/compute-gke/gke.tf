resource "google_container_cluster" "cluster" {
  name        = var.cluster_name
  location    = var.project_region
  description = var.cluster_description
  binary_authorization {
    evaluation_mode = var.cluster_binary_authorization ? "PROJECT_SINGLETON_POLICY_ENFORCE" : "DISABLED"
  }
  enable_shielded_nodes    = var.cluster_shielded_nodes
  initial_node_count       = 1
  remove_default_node_pool = true
  networking_mode          = var.cluster_networking_mode //"VPC_NATIVE"
  ip_allocation_policy {
    cluster_ipv4_cidr_block  = var.cluster_ip4_cidr         //""
    services_ipv4_cidr_block = var.services_ipv4_cidr_block //""
  }

  master_authorized_networks_config {
    dynamic "cidr_blocks" {
      for_each = var.cluster_man_needed ? var.cluster_man_rules : [] //man_rules list(map) //Add your machines ip too
      content {
        cidr_block   = cidr_blocks.value.cidr_block
        display_name = cidr_blocks.value.display_name
      }
    }
  }

  network    = var.network
  subnetwork = var.subnetwork
  release_channel {
    channel = "STABLE"
  }

  private_cluster_config {
    enable_private_nodes    = var.cluster_enable_private_nodes    //true
    enable_private_endpoint = var.cluster_enable_private_endpoint //false
    master_ipv4_cidr_block  = var.cluster_master_ipv4_cidr_block  //"172.16.0.0/28"

  }

  workload_identity_config {
    workload_pool = var.identity_namespace
  }

  lifecycle {
    ignore_changes = [ master_authorized_networks_config ]
  }
}
