output "cluster_name" {
  value = google_container_cluster.cluster.name
}

output "endpoint" {
  value = google_container_cluster.cluster.endpoint
}

output "client_certificate" {
  value = google_container_cluster.cluster.master_auth.0.client_certificate
}

output "client_key" {
  value = google_container_cluster.cluster.master_auth.0.client_key
}

output "ca_certificate" {
  value = google_container_cluster.cluster.master_auth.0.cluster_ca_certificate
}

output "id" {
  value = google_container_cluster.cluster.id
}