variable "project_id" {
  type = string
  default = "shriram-poc-project1"
}

variable "project_region" {
  type = string
  default = "us-central1"
}