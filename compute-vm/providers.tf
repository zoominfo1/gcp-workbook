provider "google" {
  project     = var.project_id
  region      = var.project_region
  zone        = "${var.project_region}-c"
}

provider "google-beta" {
  project     = var.project_id
  region      = var.project_region
}
